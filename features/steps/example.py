from behave import *
from selenium.webdriver.common.by import By

@given(u'that we are on the page named "{page}" for the category "{category}"')
def step_impl(context, page, category):
    context.basics.click(By.XPATH, f'//header[contains(@id, "menu")]//div[contains(@id, "main-nav")]//nav//ul[contains(@id, "menu-menu-marque-cesi")]/li/button[.//*[contains(text(), "{category}")]]')
    context.basics.click(By.XPATH, f'//header[contains(@id, "menu")]//div[contains(@id, "main-nav")]//nav//ul[contains(@id, "menu-menu-marque-cesi")]/li[button//*[contains(text(), "{category}")]]/ul/li/a[.//*[contains(text(), "{page}")]]')

@when(u'we search for "{search}"')
def step_impl(context, search):
    context.basics.send_keys(By.CSS_SELECTOR, '.main-search form input#main-search__input-field', search)
    context.basics.click(By.CSS_SELECTOR, '.main-search form button')

@then(u'we found diploma with title "{title}"')
def step_impl(context, title):
    context.basics.visible(By.XPATH, f'//main//div[contains(@class, "search-results")]//ul/li//h2//*[contains(text(), "{title}")]')
