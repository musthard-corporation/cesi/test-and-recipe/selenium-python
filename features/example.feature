Feature: Example

    Scenario: Search for developer diploma
        Given that we are on the page named "Informatique & Numérique" for the category "Domaines"
        When we search for "développeur"
        Then we found diploma with title "Concepteur·trice développeur·se d’applications"
