#!/usr/bin/python3.10

from behave import use_fixture
from selenium.webdriver.common.by import By
from custom_fixtures.browser import with_docker_driver, with_managed_driver
from helpers.basics import Basics


def before_all(context):
    print('> Starting the browser')
    running_platform = context.config.userdata['running_platform']

    if running_platform == 'docker':
        use_fixture(with_docker_driver, context)
    elif running_platform == 'managed':
        use_fixture(with_managed_driver, context)
    else:
        raise Exception(
            f'Unknown running_platform "{running_platform}"'
        )

    context.basics = Basics(context.driver)

    context.basics.clear_cookies()
    context.basics.open('https://www.cesi.fr/')
    try:
        context.basics.locate(By.ID, 'didomi-popup')
        print('> Popup open')
        context.basics.click(By.CSS_SELECTOR, '#didomi-popup .didomi-popup-container .didomi-popup-view .didomi-continue-without-agreeing')
    except:
        pass


def after_all(context):
    if 'basics' in context:
        context.basics.close()