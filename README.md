#

## Installation

```shell
# Install pipenv if not already available
pip install --user pipenv
```

## Exécution

```shell
sudo docker run -d -p 4444:4444 selenium/standalone-chrome

# Here parenthesis is for execute behave in the right folder
# '--no-capture' is to see steps print in the console
pipenv run behave --no-capture
```

## Help

Get current Selenium version
```shell
python3 -c "import selenium; print(selenium.__version__)"
```

Test in console
```javascript
// XPATH selector
$x('...');
// CSS selector
document.querySelector('...');
document.querySelectorAll('...');
```
