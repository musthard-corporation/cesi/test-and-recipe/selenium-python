from behave import fixture
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
import subprocess
import time


@fixture
def with_managed_driver(context):
    running_browser = context.config.userdata['running_browser']

    if running_browser == 'chrome':
        options = webdriver.ChromeOptions()
        options.add_argument("--start-maximized")
        
        context.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()),
            options=options
        )
    else:
        raise Exception(
            f'Unknown running_browser "{running_browser}" for "managed"'
        )

@fixture
def with_docker_driver(context):
    running_browser = context.config.userdata['running_browser']

    if running_browser == 'chrome':
        docker_image_name = 'selenium/standalone-chrome'

        options = webdriver.ChromeOptions()
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
    else:
        raise Exception(
            f'Unknown running_browser "{running_browser}" for "docker"'
        )

    container_name = f'selenium-{running_browser}'

    if not is_docker_image_running(docker_image_name):
        run_docker_image(docker_image_name, container_name)

    context.driver = webdriver.Remote(
        'http://127.0.0.1:4444/wd/hub', options=options
    )


def is_docker_image_running(image_name):
    result = subprocess.run(
        f'sudo docker ps | grep {image_name}', shell=True, check=False
    )

    return result.returncode == 0


def run_docker_image(image_name, container_name):
    subprocess.run(
        f'sudo docker start {container_name} || sudo docker run -d --name {container_name} -p 4444:4444 {image_name}', shell=True, check=True
    )

    counter = 0

    while not is_docker_image_running(image_name) and counter < 10:
        time.sleep(1)
        counter += 1

    if counter == 10:
        raise Exception(
            f'Docker image "{image_name}" not running'
        )