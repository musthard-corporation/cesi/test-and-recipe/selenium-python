from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.alert import Alert


class Basics():
    def __init__(self, driver, timeout = 5):
        print(f'> Init with timeout {timeout} seconds')
        self._driver = driver
        self._driver_wait = WebDriverWait(self._driver, timeout)
    

    def clear_cookies(self):
        print('> Clear cookies')
        self._driver.delete_all_cookies()
    

    def open(self, uri: str):
        print(f'> Accessing website : {uri}')
        self._driver.get(uri)
    

    def refresh(self):
        print(f'> Refreshing current page')
        self._driver.refresh()
    

    def close(self):
        print('> Closing & quitting browser')
        self._driver.close()
        self._driver.quit()
    

    def locate(self, by: By, locator: str):
        print(f'> Locate element by "{by}" with locator : {locator}')
        return self._driver_wait.until(
            expected_conditions.presence_of_element_located((by, locator))
        )


    def locate_all(self, by: By, locator: str):
        print(f'> Locate all elements by "{by}" with locator : {locator}')
        return self._driver_wait.until(
            expected_conditions.presence_of_all_elements_located((by, locator))
        )
    

    def visible(self, by: By, locator: str):
        print(f'> Find visible element by "{by}" with locator : {locator}')
        return self._driver_wait.until(
            expected_conditions.visibility_of_element_located((by, locator))
        )


    def visible_all(self, by: By, locator: str):
        print(f'> Find all visible element by "{by}" with locator : {locator}')
        return self._driver_wait.until(
            expected_conditions.visibility_of_all_elements_located((by, locator))
        )
    

    def clickable(self, by: By, locator: str):
        print(f'> Find clickable element by "{by}" with locator : {locator}')
        return self._driver_wait.until(
            expected_conditions.element_to_be_clickable((by, locator))
        )
    

    def click(self, by: By, locator: str):
        element = self.clickable(by, locator)
        print(f'>> And click')
        element.click()
    

    def send_keys(self, by: By, locator: str, value):
        element = self.locate(by, locator)
        print(f'>> And send keys : {value}')
        element.send_keys(value)
    

    def get_attribute(self, by: By, locator: str, value):
        element = self.locate(by, locator)
        print(f'>> And get attribute : {value}')
        return element.get_attribute(value)
    

    def accept_alert(self):
        print('> Accept alert')
        Alert(self._driver).accept()


    def dismiss_alert(self):
        print('> Dismiss alert')
        Alert(self._driver).dismiss()